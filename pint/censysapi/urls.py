from django.conf.urls import url
from . import views
from censysapi import views as censysapi

urlpatterns = [
	url(r'^$', censysapi.buscar_censysapi, name="buscar_censysapi"),
]
