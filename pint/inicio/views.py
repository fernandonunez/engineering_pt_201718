# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.http.response import HttpResponseRedirect


def index(request):
	if not request.user.is_anonymous():
		return HttpResponseRedirect('/login/privado/')
	else:
		return render_to_response('index.html', {})
