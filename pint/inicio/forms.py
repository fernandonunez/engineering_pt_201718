from django import forms
from django.contrib.auth.models import User
from django.forms import ModelForm
 
 
class RegistroForm(ModelForm):
    class Meta:
        model = User
        fields = ['username', 'password', 'email', 'first_name', 'last_name']
        widgets = {
            'password': forms.PasswordInput(),
        }
