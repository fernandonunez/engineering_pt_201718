# -*- coding: utf-8 -*-
from django.shortcuts import render
import requests
import sys
import time
import json
from ftplib import FTP
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response
from django.contrib.auth.decorators import login_required
from mpl_toolkits.basemap import Basemap
from django.core.files.storage import FileSystemStorage
import matplotlib.pyplot as plt
import os

from modulos.tohtml import to_msg_html, to_tabla_resultadosC_html

#importacion estandar de pandas
import pandas as pd
import numpy as np

COLUMNAS = {"ip", "autonomous_system.description", "location.continent", "location.country", "location.province", "protocols", "location.longitude", "location.latitude"}


def generar_tabla(jSON):
	# Creamos un data frame con todas las columnas y como indice entero autoincrementado
	frame = pd.DataFrame(jSON['results'], columns=COLUMNAS)
	# Eliminamos todas las columnas con NaN, excepto aquellas que tengan al menos un parametro diferente a NaN. 
	return frame.dropna(axis=1, how="all")


def get_num_paginas_totales(jSON):
	return jSON['metadata']['pages']

def get_num_pagina_actual(jSON):
	return jSON['metadata']['page']
	
def get_num_resultados(jSON):
	return jSON['metadata']['count']

def lanzar_error(codigo, cod_texto, texto):
	# Lanzar HTML
	print "\t" , codigo , ": " , cod_texto +".\n" + texto


def get_resultados_censys(query_json):
	# Constantes de codigo de errores:
	CODIGO_ERRORES = {400: 'BAD REQUEST', 403: 'UNAUTHORIZED', 404: 'NOT FOUND', 429: 'RATE LIMIT EXCEEDED', 500: 'INTERNAL SERVER ERROR'}
	# Constantes para conectarse al API REST.
	UID = "20b2d05c-5434-4657-9df6-90ef9b9e36cf"	
	SECRET = "Bzl1QY2TadyaheRqUQicHLGsNGLPQb8N"
	# URL del API REST Censys.
	API_URL = "https://www.censys.io/api/v1/search/ipv4"
	# Conectamos con el API mediante POST enviando un jSON con los parametros de búsqueda y el tipo de filtrado.
	respuesta = requests.post(API_URL, json = query_json, auth = (UID, SECRET))
	# Verificamos si el codigo de respuesta esta en la lista de errores.
	if CODIGO_ERRORES.has_key(respuesta.status_code):
		# Si es así, lanzamos el error pertinente.
		lanzar_error(respuesta.status_code, CODIGO_ERRORES[respuesta.status_code], json.loads(respuesta.text)['error'])
	else:
		# En caso contrario pasamos el diccionario con los resultados.
		return json.loads(respuesta.text)
			
			
def get_query_json(parametros_busq, pagina=1):
	return {'query' : parametros_busq,
			'page' : pagina,
			'fields':["ip", "autonomous_system.description", "location.continent", "location.country", "location.province", "protocols", "location.longitude", "location.latitude" ],
			'flatten': True
			}

parametros_busq = "21.ftp.banner.metadata.description: WS_FTP and 21.ftp.banner.metadata.version: 5.0.5"

query_json = get_query_json(parametros_busq, 1)

jSON = get_resultados_censys(query_json)


tabla_completa = generar_tabla(jSON)

########################################################################
#						A PARTIR DE AQUI 							   #
########################################################################

# Generación de mapas con PANDAS.

#Esta funcion nos da el mapa que queramos en función de las coordenadas
def mapa_seccion(nombre_arch,lugar,tabla_empleada,ax=None,llclat=-80,llclon=-180,urclat=80,urclon=180):
	#Funcion que nos crea el mapa en base a las coordenadas
	m = Basemap(projection='merc',llcrnrlat=llclat,urcrnrlat=urclat,llcrnrlon=llclon,urcrnrlon=urclon,lat_ts=0,lat_0=(llclat+urclat)/2,lon_0=(llclon+urclon)/2,resolution='c',area_thresh=100000)
	#Rasgos adicionales para añadir detalles al mapa
	m.drawcoastlines() 
	m.drawcountries()
	m.drawstates()
	#Dibujar paralelas y meridianas
	#m.drawparallels(np.arange(-90.,91.,30.)) 	
	#m.drawmeridians(np.arange(-180.,181.,60.))
	#Colorines para mapas
	#m.fillcontinents(color='coral',lake_color='aqua') 
	#m.drawmapboundary(fill_color='aqua')
	#Cálculo de la representación de puntos
	dataloc = tabla_empleada;
	x, y = m(dataloc['location.longitude'].values, dataloc['location.latitude'].values)
	m.scatter(x, y, marker=".",color="r",alpha=0.5)
	plt.title(lugar)
	plt.savefig("../mapas/"+ nombre_arch)
	plt.close() 		#Si no cerramos el plt los mapas se superponen en ejecuciones consecutivas

def subir(nombre):
	ftp_servidor = 'ftp.netting.es'
	ftp_usuario  = 'netting'
	ftp_clave    = '!Beefeater24!'
	ftp_raiz     = 'webspace/httpdocs/netting.es/mapas' # Carpeta del servidor donde queremos subir el fichero
 
	# Datos del fichero a subir
	fichero_origen = '../mapas/' # Ruta al fichero que vamos a subir
	fichero_destino = '' # Nombre que tendrá el fichero en el servidor
	nomFichero = nombre[0]
	try:
		ftp = FTP(ftp_servidor,ftp_usuario,ftp_clave)
		ftp.cwd(ftp_raiz)
		ftp.storbinary("STOR %s" % nomFichero, open("%s%s" % ("",fichero_origen + nomFichero)))
		#f.close()
		ftp.quit()
	except:
		print "No se ha podido encontrar el fichero " + fichero_origen

def to_mapas_html(lista):
	#subir
	subir(lista)
	tag_img = ""
	for img in lista:
		tag_img += '<center><img src=\'http://www.netting.es/mapas/' + img + '\'/></center>'
	print tag_img
	return tag_img 

def generar_mapas(tabla):
	# ["location.continent", "location.country", "location.province", "location.longitude", "location.latitude"]
	columnas = ["location.continent", 'location.country', "location.province", "location.longitude", "location.latitude"]
	# Creamos un DataFrame con las columnas anteriores y como indice entero autoincrementado
	df = pd.DataFrame(tabla, columns=columnas)

	# Generamos frecuencia de cada continente
	tabla_continenetes = df['location.continent'].value_counts()
	print tabla_continenetes
	print tabla_continenetes.index # Continentes encontrados
	
	# Frecuencia por paises
	tabla_paises = df['location.country'].value_counts()

	# Frecuencia por provincia
	tabla_provincia = df['location.province'].value_counts()
	
	lista_mapas = []
	
	# Borrar imagenes de /static/mapas
	#find -type f -name "map_*" -delete
	#tabla_prueba = df[["location.longitude", "location.latitude"]][(df['location.continent'] == 'Asia')]
	
	if "North America" in tabla_continenetes.index:
		tabla_coordenadas= df[["location.longitude", "location.latitude"]]
		mapa_seccion("map_US","América del Norte",tabla_coordenadas,llclon=-168.88,llclat=13.15,urclon=-53.13,urclat=71.88)
		lista_mapas.append('map_US.png')
		#Groenlandia que quedaba relegada
		mapa_seccion("map_GR","Groenlandia",tabla_coordenadas,llclon=-73.8,llclat=58.3,urclon=-8.3,urclat=84.0)
		lista_mapas.append('map_GR.png')
	'''
	if "Asia" in tabla_continenetes.index:
		tabla_coordenadas= df[["location.longitude", "location.latitude"]][(df['location.continent'] == 'Asia')]
		mapa_seccion("map_AS","Asia",tabla_coordenadas,llclon=43.0,llclat=-3.9,urclon=152.7,urclat=69.9)
		lista_mapas.append('map_AS.png')
	
	if "Europe" in tabla_continenetes.index:
		tabla_coordenadas= df[["location.longitude", "location.latitude"]][(df['location.continent'] == 'Europe')]
		mapa_seccion("map_EU","Europa",tabla_coordenadas,llclon=-31.5,llclat=34.5,urclon=74.4,urclat=70.6)
		lista_mapas.append('map_EU.png')
	
	if "Africa" in tabla_continenetes.index:
		tabla_coordenadas= df[["location.longitude", "location.latitude"]][(df['location.continent'] == 'Africa')]
		mapa_seccion("map_AF","África",tabla_coordenadas,llclon=-19.8,llclat=-39.4,urclon=60.4,urclat=39.2)
		lista_mapas.append('map_AF.png')

	if "Oceania" in tabla_continenetes.index:
		tabla_coordenadas= df[["location.longitude", "location.latitude"]][(df['location.continent'] == 'Oceania')]
		mapa_seccion("map_AUS","Oceania",tabla_coordenadas,llclon=85.03,llclat=-53.33,urclon=172.66,urclat=5.97)
		lista_mapas.append('map_AUS.png')

	if "South America" in tabla_continenetes.index:
		tabla_coordenadas= df[["location.longitude", "location.latitude"]][(df['location.continent'] == 'South America')]
		mapa_seccion("map_SA","América del Sur",tabla_coordenadas,llclon=-110.03,llclat=-56.15,urclon=-28.65,urclat=24.13)
		lista_mapas.append('map_SA.png')
	
	# Caso especial españa
	if "Spain" in tabla_paises.index:
		tabla_coordenadas= df[["location.longitude", "location.latitude"]][(df['location.country'] == 'Spain')]
		mapa_seccion("map_SP","España",tabla_coordenadas,llclon=-10.21,llclat=34.67,urclon=4.65,urclat=43.79)
		lista_mapas.append('map_SP.png')
	'''
	# Generar html
	#return to_mapas_html(lista_mapas)
	
	return "","tabla_prueba"
	

	

	
mapas_html = generar_mapas(tabla_completa)



#Primera vez
print "Primer mapashtml" , mapas_html

query_json = get_query_json(parametros_busq, 2)

jSON = get_resultados_censys(query_json)


tabla_completa = generar_tabla(jSON)
mapas_html = generar_mapas(tabla_completa)



#Primera vez
print "Segundo mapashtml" , mapas_html
