# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from inicio.forms import RegistroForm
from modulos.tohtml import to_msg_html          

def RegistroUsuario(request):
    if request.method == 'POST':  # Si el formulario ha sido enviado mediante POST...
        form = RegistroForm(request.POST)  # Un formulario vinculado a los datos POST
        if form.is_valid():
			#Procesar los datos en form.cleaned_data
			username = form.cleaned_data["username"]
			password = form.cleaned_data["password"]
			email = form.cleaned_data["email"]
			first_name = form.cleaned_data["first_name"]
			last_name = form.cleaned_data["last_name"]
            
			# En este punto, el usuario es un objeto de usuario que ya se ha guardado
			# A la base de datos. Puede seguir cambiando sus atributos
			# Si desea cambiar otros campos.
			user = User.objects.create_user(username, email, password)
			user.first_name = first_name
			user.last_name = last_name
            
			# Guardar nuevos atributos de usuario
			return to_msg_html("Usuario registrado con éxito", "green")   
	
	return to_msg_html("Error. Usuario no registrado", "red")     
