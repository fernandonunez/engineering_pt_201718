from django.conf.urls import url, include
from . import views
from registro.views import RegistroUsuario

urlpatterns = [
    url(r'^$', views.RegistroUsuario, name='registro'),
]
