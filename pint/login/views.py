# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.http import HttpResponseRedirect

from modulos.tohtml import to_msg_html

from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required

def login_page(request):
	if not request.user.is_anonymous():
		return HttpResponseRedirect('/login/privado/')
		
	if request.method == 'POST':
		formulario = AuthenticationForm(data=request.POST) #Sin el data no funciona. Parace ser que sobreescribe el metodo.
		if formulario.is_valid:
			usuario = request.POST['username']
			passw = request.POST['password']
			acceso = authenticate(username=usuario, password=passw)
			if acceso is not None:
				if acceso.is_active:
					login(request, acceso)
					return HttpResponseRedirect('/login/privado/')
				else:
					return to_msg_html("User deactivated", "red")
			else:
				return to_msg_html("Access denied", "red")
		else:
			return to_msg_html("Error. Invalid form", "red")
	
	return to_msg_html("Error. No parameters received through POST request", "red")

@login_required(login_url='/')
def privado(request):
	usuario = request.user
	return render_to_response('privado.html', {'usuario':usuario},)
                              
def logout(request):
	logout(request)
	return render_to_response('index.html', {},)
