from django.conf.urls import url
from . import views
from login import views as login_views

urlpatterns = [
    url(r'^$', login_views.login_page, name="login"),
    url(r'^privado/',login_views.privado),
]
