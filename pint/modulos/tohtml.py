# -*- coding: utf-8 -*-
#importacion estandar de pandas
import pandas as pd
import numpy as np

from django.shortcuts import render_to_response

def to_msg_html(msg, color):
	return render_to_response('mensajes.html',{'msg':msg, 'color':color})

#-----------------------------------------------------------------------

def to_formato(valor):
	if valor in ['nan', "nan"]:
		return ""
	if type(valor) == list:
		if len(valor)>1:
			valor = ", ".join(valor)
		else:
			valor = "".join(valor)
	return str(valor)

#-----------------------------------------------------------------------
def to_tabla_resultadosC_html(clase_css, tabla):
	# Inicializamos cadena a vacio.
	tabla_completa = ""
	# Seleccionamos las columnas que vamos a usar.
	columnas_resultado = {"ip", "autonomous_system.description", "location.continent", "location.country", "location.province", "protocols"}
	df = pd.DataFrame(tabla, index=[range(len(tabla))], columns=columnas_resultado)
	# Recorremos por index y accedemos al valor de cada columna
	for ids in df.index:
		direccion_ip = str(df.loc[ids,'ip'])
		nombre = str(df.loc[ids,'autonomous_system.description'])
		continente = str(df.loc[ids,'location.continent'])
		pais = str(df.loc[ids,'location.country'])
		provincia = str(df.loc[ids,'location.province'])#.encode('ascii', 'ignore')
		protocolos = to_formato((df.loc[ids,'protocols']))
		
		comienzo = "<table class=\"" + "tablaFuera" + "\"><tbody><tr><td><table class=\"" + clase_css + "\"><tbody><tr><td>"
		datos = "<table class=\"tablaResultadoCensys center\"><tbody><tr><td class=\"tablaResultadoCensys tde-izq\"><ntext>Name: </ntext>" + nombre + "</td><td class=\"tablaResultadoCensys tde-derch\"><ntext>IP: </ntext>" + direccion_ip + "</td></tbody></table></tr></td></tr>"
		proto = "<tr><td><ntext>Port & protocol: </ntext>" + protocolos + "</td></tr>"
		loc = "<tr><td><ntext>Location: </ntext>" + continente + ", " + pais + ", " + provincia + "</td></tr>"
		fin = "</td></tr></tbody></table></td></tr></tbody></table>"
		tabla_completa = tabla_completa + (comienzo +  datos + loc + proto + fin)
	return tabla_completa

#-----------------------------------------------------------------------

def to_tabla_resultados_html(clase_css, tabla):
	# Inicializamos cadena a vacio.
	tabla_completa = ""
	# Seleccionamos las columnas que vamos a usar.
	columnas_resultado = ['author', 'source', 'date', 'description', 'platform', 'port', 'title', 'type', "_id"]
	# A partir de las columnas seleccionadas creamos una nueva tabla.
	df = pd.DataFrame(tabla, index=tabla.index, columns=columnas_resultado)
	# Recorremos por index y accedemos al valor de cada columna
	for _id in df.index:
		#description = to_formato(str(df.loc[_id,'description']))
		description = str(df.loc[_id,'title'])
		if description in ["", " ", 'nan']:
			description = str(df.loc[_id,'description'])
		
		platform = to_formato(df.loc[_id,'platform'])
		tipe = to_formato(str(df.loc[_id,'type']))
		port = to_formato(str(df.loc[_id,'port']))
		source = to_formato(str(df.loc[_id,'source']))
		author = to_formato(df.loc[_id,'author'])
		date = to_formato(str(df.loc[_id,'date'])[0:10])
		
		#title = str(df.loc[_id,'title'])
		url = "https://www.exploit-db.com/exploits/" + str(_id)
		
		comienzo = "<table class=\"" + clase_css + "\"><tbody><tr><td><a href=\"" + url + "\">" + description + "</a></td></tr>"
		medio = "<tr><td><table class=\"tablaResultadoInterior\"><tbody><tr><td class=\"tablaResultadoInterior td-fecha\"><ntext>Fecha: </ntext>" + date + "</td><td class=\"tablaResultadoInterior td-plataforma\"><ntext>Autor: </ntext>" + author + "</td></tbody></table></td></tr>"
		fin = "<tr><td><table class=\"tablaResultadoInterior\"><tbody><tr><td><ntext>Fuente: </ntext>" + source + "</td><td><ntext>  Tipo: </ntext>" + tipe + "</td><td><ntext>Puerto: </ntext>" + port + "</td><td class=\"tablaResultadoInterior td-plataforma\"><ntext>Plataforma: </ntext>" + platform + "</td></tr></tbody></table></td></tr><tr><td>    </td></tr></tbody></table>"
		
		tabla = comienzo + medio + fin
		tabla_completa = tabla_completa + tabla

	return tabla_completa
	
#-----------------------------------------------------------------------

def to_tabla_clasificacion_html_recursiva(clase_css, lista):
	if lista == []:
		return ""
	else:
		laux = lista.pop(0)
		comienzo = str("<table class=\"" + clase_css + "\"><tbody><tr><th colspan=\"2\">" + laux[0] + "</th></tr>")
		medio = ""
		for i in range(len(laux[1][0])):
			medio = medio + "<tr><td class=\"tablaCategorias td-izq\">" + str(laux[1][0][i]) + "</td><td class=\"tablaCategorias td-derch\">" + str(laux[1][1][i]) + "</td></tr>"
		fin = str("</tbody></table>")
		aux = comienzo + str(medio) + fin
		
		return aux + to_tabla_clasificacion_html_recursiva(clase_css, lista)
		
#-----------------------------------------------------------------------		
