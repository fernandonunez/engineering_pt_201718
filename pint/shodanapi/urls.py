from django.conf.urls import url
from . import views
from shodanapi import views as shodanapi

urlpatterns = [
	url(r'^$', shodanapi.buscar_shodanapi, name="buscar_shodanapi"),
]
