# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
import requests
import sys
import json
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response
from django.contrib.auth.decorators import login_required

from modulos.tohtml import to_msg_html, to_tabla_clasificacion_html_recursiva, to_tabla_resultados_html

#importacion estandar de pandas
import pandas as pd
import numpy as np


#///////////////////////////////
#////  C O N S T A N T E S  ////
#///////////////////////////////

CATEGORIA = {'author', 'platform', 'port', 'source', 'type'}
#CATEGORIA = {'author', 'platform', 'source', 'type'}
COLUMNAS = ['author', 'source', 'bid', 'code', 'cve', 'date', 'description', 'msb', 'osvdb', 'platform', 'port', 'title', 'type', "_id"]



##############################################################################################

def get_paginacion(pag_actual, pag_totales, full_path):
	# Obtenemos la ruta sin la varible page
	url = full_path[:full_path.find("page")]
	html = ""
	for i in range(1, int(pag_totales+1)):
		pag = str(i)
		if i == (pag_actual):
			pag = " [ " + str(i) + " ] "
		html = html + "<a href=\"" + url + "page=" + str(i) + "\"> " + pag + " </a>"
	return html

##############################################################################################

def get_URL(consulta, parametros, pagina):
	#API_KEY
	SHODAN_API_KEY = "I8yALOxPesTrTbpBdcJCyGVrgO8sjM4M"
	#CONSULTA: search o count
	#Query
	query = consulta + "?query=" + parametros + "&page=" + pagina
	return "https://exploits.shodan.io/api/" + query + "&key=" + SHODAN_API_KEY

##############################################################################################

def get_parametros(request, metodo, nompara, page):
	valor = []
	if request.method == metodo:  # Si el formulario ha sido enviado mediante METODO...
		valor.append(request.GET[nompara])
		valor.append(request.GET[page])
		return valor
	else:
		return None
	

##############################################################################################

def get_respuesta(requests, URL):
	r = requests.get(URL)
	if r.status_code == 200:
		try:
			#Parseamos de string a jSON
			#jsonBusq = r.json()
			jSONBusq = json.loads(r.text)
			return jsonBusq, None
		except:
			return None, to_msg_html("Error parsing JSON. " + str(sys.exc_info()[0]) + URL, "red")
	else:
		return None, to_msg_html("Error searching exploits. SearchError ID: " + str(r.status_code), "red")
	
##############################################################################################

#----------------------------------------------------------------------------------------------

def get_num_paginas_totales(num_resultados):
	if num_resultados <= 100:
		return 1
	
	res = num_resultados / 100
	if (num_resultados % 100) != 0:
		res +=1 
	return res

#----------------------------------------------------------------------------------------------

def get_numresultados(jSON):
	return jSON['total']

#----------------------------------------------------------------------------------------------
	
def generar_tabla(jSON):
	# Creamos un data frame con todas las columnas y como indice _id
	frame = pd.DataFrame(jSON['matches'], columns=COLUMNAS).set_index('_id')
	# Eliminamos todas las columnas con NaN, excepto aquellas que tengan al menos un parametro diferente a NaN. 
	return frame.dropna(axis=1, how="all")

#----------------------------------------------------------------------------------------------

def get_categoria(parametros):
	v = dividir_parametros(parametros)
	# Pasamos la lista a conjunto.
	tupla = set(v)
	# Devolvemos la interseccion como conjunto.
	return CATEGORIA & tupla

#...............................................................................................

def dividir_parametros(parametros):
	# Si contiene : se los quitamos
	if parametros.find(":") >= 0:
		#Reemplazamos los : por espacios. [Si los eliminamos juntamos palabras]
		parametros=parametros.replace(":"," ")
	# Separamos los parametros por espacios. Da igual la cantidad.
	v = parametros.split()
	return v

#----------------------------------------------------------------------------------------------

def to_nombreEs(nombre):
	if nombre == 'author':
		return 'Author'
	if nombre == 'platform':
		return 'Plataform'
	if nombre == 'port':
		return 'Port'		
	if nombre == 'source':
		return 'Source'
	if nombre == 'type':
		return 'Type'

#..............................................................................................	

def to_lista_unica(l):
	aux = []
	for i in range(len(l)):
		if type(l[i]) != list:
			aux.append(l[i])
		else:
			aux = aux + l[i]
	return aux

#..............................................................................................		

# Recibe una serie.
def estadarizar_jSON(df, nombre):
	df = df.dropna(how='any')
	lista = df[nombre].tolist()
	lista = to_lista_unica(lista)
	if nombre in ['port']:
		for i in range(len(lista)):
			lista[i] = int(lista[i])
	if nombre in ['platform', 'author', 'type']:
		for i in range(len(lista)):
			lista[i] = str(lista[i])
	aux = pd.DataFrame({nombre : lista})
	# Calculamos las frecuencias de cada categoria es una serie
	return aux[nombre].value_counts()

#..............................................................................................			
		
#Devuelve una Lista que contiene listas. Estas listas tienen un string y otra lista.
def crear_listaclasificacion(resultado, tabla):
	#Inicializamos listas.
	lista_clasificacion=[]
	aux = pd.DataFrame()
	#calcula las frecuencias de cada categoria y las agrupamos.
	for nombre in resultado:
		# Estandarizamos a listas todas las columnas.
		# Se pasa como serie.
		aux = estadarizar_jSON(pd.DataFrame({nombre:tabla[nombre]}), nombre)
		#La serie esta formada por las columnas (que son los indices y los valores que son las repeticiones)
		
		#		+-------+---------+-----+
		#		| linux | windows | ... |  <---- Columnas
		#		+-------+---------+-----+
		#		|     3 |      10 | ... | <----- valores
		#		+-------+---------+-----+
		
		#Pasamos las columnas a una lista
		columna = aux.keys().tolist()
		#Pasamos los valores a una lista
		valor = aux.tolist()
		#Para no mostrar todos los autores, creamos un top 5.
		if len(aux) > 10 and nombre == "author":
			valor = valor[0:10]
			columna = columna[0:10]
		#Pasamos el nombre a castellano
		nombre = to_nombreEs(nombre)
		#Añadimos TituloCategoria y la lista con las frecuencias como lista de columna y valor.
		lista_clasificacion.append([nombre, [columna, valor]])
	return lista_clasificacion
	
	#Estructura
	
	#ListaPrincipal |---> ListaSecundaria 1 |---> TituloLista     |---> Nombre (ejemplo: Windows, linux, etc)
	#				|					    |---> ListaFrecuencia |---> Entero (La frecuencia)
	#               |
	#				|---> ListaSecundaria 2 |---> TituloLista     |---> Nombre (ejemplo: Windows, linux, etc)
	#				|	     			    |---> ListaFrecuencia |---> Entero (La frecuencia)		
	#				|
	#				|---> ListaSecundaria n |---> TituloLista     |---> Nombre (ejemplo: Windows, linux, etc)
	#				|	     			    |---> ListaFrecuencia |---> Entero (La frecuencia)		
	
	
#..............................................................................................		
		
def get_clasificacion(tabla, categorias_busqueda):
	# Casteamos las columas a tupla
	tuplaCol = set(tabla.columns)
	# Interseccion y Union exclusiva entre los campos y los campos devueltos en la búsqueda y la categoria.
	resultado = (CATEGORIA & tuplaCol) ^ categorias_busqueda
	# Creamos la lista de clasificacion.
	return crear_listaclasificacion(resultado, tabla)
	

#----------------------------------------------------------------------------------------------
			
@login_required(login_url='/')
def buscar_shodanapi(request):
	try:
		#print >>sys.stderr, request.GET
		# Obtenemos parametros.
		parametros = get_parametros(request, 'GET', 'buscarShodan', 'page')
		# Generamos la URL para lanzar al API
		URL = get_URL("search", parametros[0], parametros[1])
		# Obtenemos la búsqueda como jSON sin errores.
		# jSONBusq, error = get_respuesta(requests, URL)
		# if jSONBusq == None:
			#raise Exception
		r = requests.get(URL)
		jSONBusq = json.loads(r.text)
	except:
		return to_msg_html("Error retrieving data. Server unreachable", "red")
		
	try:
		# Numero de resultados obtenidos.
		num_resultados = get_numresultados(jSONBusq)
		
		# Si no hay resultados paramos.
		if num_resultados == 0:
			txt = "No results found."
			return render_to_response('tabla1.html', {'NumResultados': txt},)
		
		# Generamos la tabla "completa" con indices. [Panda]
		tabla_completa = generar_tabla(jSONBusq)
	
		# Obtenemos las categorias en la busqueda web.
		categorias_busqueda = get_categoria(parametros[0])
		
		# Obtenemos la clasificación de las búsquedas. [Lista de listas de cadena y lista]
		lista_clasificacion = get_clasificacion(tabla_completa, categorias_busqueda)
		
		# Convertimos la clasificación de categorias por frecuencias a table html
		html_categoria = to_tabla_clasificacion_html_recursiva("tablaCategorias", lista_clasificacion)
		
		##tabla_resultado = generar_tabla_resultados(jSONBusq)
		# Convertimos los resultados en html
		html_resultados = to_tabla_resultados_html('tablaResultado', tabla_completa)
		
		#Concatenamos el texto con el numero de resultados.
		str_num_resultados = "Results found: " + str(num_resultados) + "."
		
		# Obtenemos el numero de paginas totales
		pag_totales = get_num_paginas_totales(num_resultados)
		
		# Obtener pagina actual
		pag_actual = int(parametros[1])
		
		print >>sys.stderr, "pag_actual " + str(pag_actual)
		
		# Generamos el numero de páginas
		lista_paginas_html = get_paginacion(pag_actual, pag_totales, request.get_full_path())
		
		#Concatenamos el texto con el numero de resultados.
		str_paginas = " Page: " + str(pag_actual) + " out of " + str(pag_totales) + "."
		
		return render_to_response('tabla1.html', {'tablaCat': html_categoria, 'NumResultados': str_num_resultados, 'tablaRes': html_resultados, 'tabCensys': False, 'PaginaResultados': lista_paginas_html, "PaginasShodan": str_paginas },)
	except:
		usuario = request.user
		return render_to_response('privado.html', {'usuario':usuario},)
	
