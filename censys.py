# -*- coding: utf-8 -*-
import json
import requests


def get_num_paginas_totales(jSON):
	return jSON['metadata']['pages']

def get_num_pagina_actual(jSON):
	return jSON['metadata']['page']
	
def get_num_resultados(jSON):
	return jSON['metadata']['count']

def lanzar_error(codigo, cod_texto, texto):
	# Lanzar HTML
	print "\t" , codigo , ": " , cod_texto +".\n" + texto


def get_resultados_censys(query_json):
	# Constantes de codigo de errores:
	CODIGO_ERRORES = {400: 'BAD REQUEST', 403: 'UNAUTHORIZED', 404: 'NOT FOUND', 429: 'RATE LIMIT EXCEEDED', 500: 'INTERNAL SERVER ERROR'}
	# Constantes para conectarse al API REST.
	UID = "20b2d05c-5434-4657-9df6-90ef9b9e36cf"	
	SECRET = "Bzl1QY2TadyaheRqUQicHLGsNGLPQb8N"
	# URL del API REST Censys.
	API_URL = "https://www.censys.io/api/v1/search/ipv4"
	# Conectamos con el API mediante POST enviando un jSON con los parametros de búsqueda y el tipo de filtrado.
	respuesta = requests.post(API_URL, json = query_json, auth = (UID, SECRET))
	# Verificamos si el codigo de respuesta esta en la lista de errores.
	if CODIGO_ERRORES.has_key(respuesta.status_code):
		# Si es así, lanzamos el error pertinente.
		lanzar_error(respuesta.status_code, CODIGO_ERRORES[respuesta.status_code], json.loads(respuesta.text)['error'])
	else:
		# En caso contrario pasamos el diccionario con los resultados.
		return json.loads(respuesta.text)
			
			
def get_query_json(parametros_busq, pagina=1):
	return {'query' : parametros_busq,
			'page' : pagina,
			'fields':["ip", "autonomous_system.description", "location.continent", "location.country", "location.province", "protocols"],
			'flatten': False
			}

parametros_busq = "21.ftp.banner.metadata.description: warFTPd and 21.ftp.banner.metadata.version: 1.65"

query_json = get_query_json(parametros_busq)

jSON = get_resultados_censys(query_json)

#print jSON

pag_totales = get_num_paginas_totales(jSON)
pag_actual = get_num_pagina_actual(jSON)	
num_resultados = get_num_resultados(jSON)

print "resultados: " , num_resultados
print "Pagina " , pag_actual, " de ", pag_totales
