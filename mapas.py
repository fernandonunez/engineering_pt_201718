# -*- coding: utf-8 -*-
import json
import requests

#importacion estandar de pandas
import pandas as pd
from pandas import DataFrame, Series
import numpy as np

COLUMNAS = {"ip", "autonomous_system.description", "location.continent", "location.country", "location.province", "protocols", "location.longitude", "location.latitude"}


def generar_tabla(jSON):
	# Creamos un data frame con todas las columnas y como indice entero autoincrementado
	frame = pd.DataFrame(jSON['results'], columns=COLUMNAS)
	# Eliminamos todas las columnas con NaN, excepto aquellas que tengan al menos un parametro diferente a NaN. 
	return frame.dropna(axis=1, how="all")


def get_num_paginas_totales(jSON):
	return jSON['metadata']['pages']

def get_num_pagina_actual(jSON):
	return jSON['metadata']['page']
	
def get_num_resultados(jSON):
	return jSON['metadata']['count']

def lanzar_error(codigo, cod_texto, texto):
	# Lanzar HTML
	print "\t" , codigo , ": " , cod_texto +".\n" + texto


def get_resultados_censys(query_json):
	# Constantes de codigo de errores:
	CODIGO_ERRORES = {400: 'BAD REQUEST', 403: 'UNAUTHORIZED', 404: 'NOT FOUND', 429: 'RATE LIMIT EXCEEDED', 500: 'INTERNAL SERVER ERROR'}
	# Constantes para conectarse al API REST.
	UID = "20b2d05c-5434-4657-9df6-90ef9b9e36cf"	
	SECRET = "Bzl1QY2TadyaheRqUQicHLGsNGLPQb8N"
	# URL del API REST Censys.
	API_URL = "https://www.censys.io/api/v1/search/ipv4"
	# Conectamos con el API mediante POST enviando un jSON con los parametros de búsqueda y el tipo de filtrado.
	respuesta = requests.post(API_URL, json = query_json, auth = (UID, SECRET))
	# Verificamos si el codigo de respuesta esta en la lista de errores.
	if CODIGO_ERRORES.has_key(respuesta.status_code):
		# Si es así, lanzamos el error pertinente.
		lanzar_error(respuesta.status_code, CODIGO_ERRORES[respuesta.status_code], json.loads(respuesta.text)['error'])
	else:
		# En caso contrario pasamos el diccionario con los resultados.
		return json.loads(respuesta.text)
			
			
def get_query_json(parametros_busq, pagina=1):
	return {'query' : parametros_busq,
			'page' : pagina,
			'fields':["ip", "autonomous_system.description", "location.continent", "location.country", "location.province", "protocols", "location.longitude", "location.latitude" ],
			'flatten': True
			}

parametros_busq = "21.ftp.banner.metadata.description: warFTPd and 21.ftp.banner.metadata.version: 1.65"

query_json = get_query_json(parametros_busq)

jSON = get_resultados_censys(query_json)


tabla_completa = generar_tabla(jSON)

########################################################################
#						A PARTIR DE AQUI 							   #
########################################################################

def to_mapas_html(lista):
	tag_img = ""
	for img in lista:
		tag_img += '<center><img src=\'../static/mapas/' + img + '\'/></center>'
	print tag_img
	return tag_img 

def generar_mapas(tabla):
	# ["location.continent", "location.country", "location.province", "location.longitude", "location.latitude"]
	columnas = ["location.continent", 'location.country', "location.province", "location.longitude", "location.latitude"]
	# Creamos un DataFrame con las columnas anteriores y como indice entero autoincrementado
	df = pd.DataFrame(tabla, columns=columnas)
	# Generamos frecuencia de cada continente
	tabla_continenetes = df['location.continent'].value_counts()
	print tabla_continenetes
	print tabla_continenetes.index # Continentes encontrados
	
	# Frecuencua por paises
	tabla_paises = df['location.country'].value_counts()
	print "\n\n"
	print tabla_paises
	print tabla_paises.index # Paises encontrados
	
	# Frecuencia por provincia
	tabla_provincia = df['location.province'].value_counts()
	print "\n\n"
	print tabla_provincia # Puede ser vacio
	print tabla_provincia.index
	
	# Filtrado por:
	# Continente
	print "\n\n"	
	tabla_coordenadas_asia = df[["location.longitude", "location.latitude"]][(df['location.continent'] == tabla_continenetes.index[0])]
	print tabla_coordenadas_asia
	
	#print tabla_coordenadas
	
	# Xogando con todo esto, o normal e que por continente agora fagas:
	# Si utilizamos ifs con listas evitamos hacer bucle
	
	lista_mapas = []
	
	# Borrar imagenes de /static/mapas
	# find -type f -name "*mipatron*.txt" -delete
	
	# ¡Ojo! Nombres tal cuales de censys.
	
	if "North America" in tabla_continenetes.index:
		# LLamas a la funcion que genera los mapas. Pasandole:
		#						El mapa a utilizar. En este caso america.
		#						Los puntos a pintar. 
		#							tabla_coordenadas= df[["location.longitude", "location.latitude"]][(df['location.continent'] == 'North America')]
		#						Nombre del fichero a salvar. ruta ../static/mapas/
		# 	lista_mapas("user_nombre_del_fichero_guardado.jpg/png/etc")
		pass
	
	if "Asia" in tabla_continenetes.index:
		# idem
		pass
	
	#...
	
	# Caso especial españa
	
	if "Spain" in tabla_paises.index:
		# Calculamos los puntos de españa.
		# Misma historia
		pass
		
	# Generar html
	return to_mapas_html(['logo.png','logo.png','logo.png'])# return to_mapas_html(lista_mapas)
	

	
	
	
mapas_html = generar_mapas(tabla_completa)
